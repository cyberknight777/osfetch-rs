# osfetch-rs v0.1.0

![logo](img/logo.png)
A **speed-focused**(**<1.8ms** execution time) fetch utility written in *Rust*.

> To install after cloning :
```bash
cd osfetch-rs
make
sudo make install
```
Or you could install from crates.io via this: 
```bash
cargo install osfetch-rs
```

You could install via AUR if you're an Arch/Arch-based user via this:
```bash
<your favourite aur helper> <installation flag> osfetch-rs
```

## Screenshots

Arch: 

![arch](img/arch.png)


Manjaro: 

![manjaro](img/manjaro.png)


Artix: 

![artix](img/artix.png)

## Inspired by

+ [neofetch](https://github.com/dylanaraps/neofetch)

> Thanks to my testers

+ [chroot](https://gitlab.com/primalkaze)
+ [Mo](https://gitlab.com/ouroboros420)

Thanks to [hridyansh](https://gitlab.com/acnologia000) for helping in parsing arguments
