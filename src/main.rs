use nixinfo;
use std::{env, process};
use whoami;

#[cfg(target_os = "android")]
fn main() {
    let args: Vec<_> = env::args().collect();
    let distro = nixinfo::distro();
    let device = nixinfo::device();
    let kernel_cmd = &process::Command::new("uname")
        .arg("-r")
        .output()
        .expect("failed to get kernel version");
    let kernel = String::from_utf8_lossy(&kernel_cmd.stdout);
    let uptime_cmd = &process::Command::new("uptime")
        .arg("--pretty")
        .output()
        .expect("failed to get uptime");
    let uptime = String::from_utf8_lossy(&uptime_cmd.stdout);
    let mem = nixinfo::memory();
    let cpu = nixinfo::cpu();
    let term = nixinfo::env("TERM");
    let host = nixinfo::hostname();
    let name = whoami::username();
    let shell = nixinfo::env("SHELL");
    if args.len() > 1 && args[1].len() == 8 {
        print!(
            "
{}@{}
----------------
OS: {}
Host: {}
Kernel: {}
Shell: {}
Uptime: {}
CPU: {}
Term: {}
RAM: {}
",
            &name.replace("()", ""),
            &host.unwrap(),
            &distro.unwrap().replace("\"", ""),
            &device.unwrap(),
            &kernel.trim(),
            &shell.unwrap().replace(concat!(env!("PREFIX"), "/bin/"), ""),
            &uptime.trim().replace("up", ""),
            &cpu.unwrap(),
            &term.unwrap(),
            &mem.unwrap(),
        );
        process::exit(0);
    } else if args.len() > 1 && args[1].len() < 8 {
        println!(
            "\x1b[1mSupported arguments:- 
                  --stdout ~ Turn off all colors and disables the ANSI backend.\x1b[0m"
        );
        process::exit(1);
    } else {
        print!("\n                       \x1b[1;37m{}@{}\x1b[0m
        \x1b[1;40;30m#####\x1b[0m          \x1b[1;39m----------------\x1b[0m  
       \x1b[1;40;30m#######\x1b[0m         \x1b[1;36mOS: {}\x1b[0m
       \x1b[1;40;30m##\x1b[0m\x1b[1;43;33mO\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;43;33mO\x1b[0m\x1b[1;40;30m##\x1b[0m         \x1b[1;32mHost: {}\x1b[0m
       \x1b[1;40;30m#\x1b[0m\x1b[1;43;33m#####\x1b[0m\x1b[1;40;30m#\x1b[0m         \x1b[1;34mKernel: {}\x1b[0m
     \x1b[1;40;30m##\x1b[0m\x1b[1;47;37m##\x1b[0m\x1b[1;43;33m###\x1b[0m\x1b[1;47;37m##\x1b[0m\x1b[1;40;30m##\x1b[0m       \x1b[1;35mShell: {}\x1b[0m
    \x1b[1;40;30m#\x1b[0m\x1b[1;47;37m##########\x1b[0m\x1b[1;40;30m##\x1b[0m      \x1b[1;33mUptime: {}\x1b[0m
   \x1b[1;40;30m#\x1b[0m\x1b[1;47;37m############\x1b[0m\x1b[1;40;30m##\x1b[0m     \x1b[1;31mCPU: {}\x1b[0m
   \x1b[1;40;30m#\x1b[0m\x1b[1;47;37m############\x1b[0m\x1b[1;40;30m###\x1b[0m    \x1b[1;95mTerm: {}\x1b[0m
  \x1b[1;43;33m##\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;47;37m###########\x1b[0m\x1b[1;40;30m##\x1b[0m\x1b[1;43;33m#\x1b[0m    \x1b[1;96mRAM: {}\x1b[0m
\x1b[1;43;33m######\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;47;37m#######\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;43;33m######\x1b[0m  \x1b[1;94m\x1b[0m
\x1b[1;43;33m#######\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;47;37m#####\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;43;33m#######\x1b[0m  \x1b[1;92m\x1b[0m
  \x1b[1;43;33m#####\x1b[0m\x1b[1;40;30m#######\x1b[0m\x1b[1;43;33m#####\x1b[0m    \x1b[1;93m\x1b[0m
\n", name, host.unwrap(), distro.unwrap().replace("\"", "").replace("()", ""), device.unwrap(), kernel.trim(), shell.unwrap().replace(concat!(env!("PREFIX"), "/bin/"), ""), uptime.trim().replace("up", ""), cpu.unwrap(), term.unwrap(), mem.unwrap());
    }
}


#[cfg(not(target_os = "android"))]
fn main() {
    let args: Vec<_> = env::args().collect();
    let distro = nixinfo::distro();
    let device = nixinfo::device();
    let kernel = nixinfo::kernel();
    let uptime = nixinfo::uptime();
    let wm = nixinfo::environment();
    let mem = nixinfo::memory();
    let cpu = nixinfo::cpu();
    let gpu = nixinfo::gpu();
    let term = nixinfo::terminal();
    let temp = nixinfo::temp();
    let host = nixinfo::hostname();
    let name = whoami::username();
    let shell = nixinfo::env("SHELL");
    if  args.len() > 1 && args[1].len() == 8 {
        print!("
{}@{}
----------------
OS: {}
Host: {}
Kernel: {}
Shell: {}
Uptime: {}
WM: {}
CPU: {}
GPU: {}
RAM: {}
Term: {}
Temp: {}°C
", &name, &host.unwrap(), &distro.unwrap().replace("\"", ""), &device.unwrap(), &kernel.unwrap(), &shell.unwrap().replace("/usr/bin/", ""), &uptime.unwrap(), &wm.unwrap(), &cpu.unwrap(), &gpu.unwrap().replace("(rev ea)03", ""), &mem.unwrap(), &term.unwrap(), &temp.unwrap());
        process::exit(0);
    } else if args.len() > 1 && args[1].len() < 8 {
        println!("\x1b[1mSupported arguments:-
                  --stdout ~ Turn off all colors and disables the ANSI backend.\x1b[0m");
        process::exit(1);
    } else {
    print!("\n                       \x1b[1;37m{}@{}\x1b[0m
        \x1b[1;40;30m#####\x1b[0m          \x1b[1;39m----------------\x1b[0m
       \x1b[1;40;30m#######\x1b[0m         \x1b[1;36mOS: {}\x1b[0m
       \x1b[1;40;30m##\x1b[0m\x1b[1;43;33mO\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;43;33mO\x1b[0m\x1b[1;40;30m##\x1b[0m         \x1b[1;32mHost: {}\x1b[0m
       \x1b[1;40;30m#\x1b[0m\x1b[1;43;33m#####\x1b[0m\x1b[1;40;30m#\x1b[0m         \x1b[1;34mKernel: {}\x1b[0m
     \x1b[1;40;30m##\x1b[0m\x1b[1;47;37m##\x1b[0m\x1b[1;43;33m###\x1b[0m\x1b[1;47;37m##\x1b[0m\x1b[1;40;30m##\x1b[0m       \x1b[1;35mShell: {}\x1b[0m
    \x1b[1;40;30m#\x1b[0m\x1b[1;47;37m##########\x1b[0m\x1b[1;40;30m##\x1b[0m      \x1b[1;33mUptime: {}\x1b[0m
   \x1b[1;40;30m#\x1b[0m\x1b[1;47;37m############\x1b[0m\x1b[1;40;30m##\x1b[0m     \x1b[1;31mWM: {}\x1b[0m
   \x1b[1;40;30m#\x1b[0m\x1b[1;47;37m############\x1b[0m\x1b[1;40;30m###\x1b[0m    \x1b[1;95mCPU: {}\x1b[0m
  \x1b[1;43;33m##\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;47;37m###########\x1b[0m\x1b[1;40;30m##\x1b[0m\x1b[1;43;33m#\x1b[0m    \x1b[1;96mGPU: {}\x1b[0m
\x1b[1;43;33m######\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;47;37m#######\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;43;33m######\x1b[0m  \x1b[1;94mRAM: {}\x1b[0m
\x1b[1;43;33m#######\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;47;37m#####\x1b[0m\x1b[1;40;30m#\x1b[0m\x1b[1;43;33m#######\x1b[0m  \x1b[1;92mTerm: {}\x1b[0m
  \x1b[1;43;33m#####\x1b[0m\x1b[1;40;30m#######\x1b[0m\x1b[1;43;33m#####\x1b[0m    \x1b[1;93mTemp: {}°C\x1b[0m
\n", name, host.unwrap(), distro.unwrap().replace("\"", ""), device.unwrap(), kernel.unwrap(), shell.unwrap().replace("/usr/bin/", ""), uptime.unwrap(), wm.unwrap(), cpu.unwrap(), gpu.unwrap().replace("(rev ea)03", ""), mem.unwrap(), term.unwrap(), temp.unwrap());
    }
}

